  /**
  * Classe "UserInfo"
  *
  *  Cette classe permet d'établir et reception les information utilisateurs
  *
  *  attribut :
  *
  *    - String username : nom d'utilisateur
  *    - String password; mot de passe utilisateur
  *   - String firstName; Prénom de l'utilisateur
  *   - String lastName; Nom de Famille de l'utilisateur
  *   - String email :  email de l'utilisateur
  *   - String role : Role de l'utilisateur
  *   - int userId : Id de l'utilisatur
  *   - String accessToken; clé d'accès utilisateur
  */

class UserInfo{

  //Login DTO
  String username;
  String password;

  //UserDetail
  String firstName;
  String lastName;
  String email;
  String role;
  int userId;
  String accessToken;

  ///Setter
  void setRole(String role) => this.role = role;


  ///Constructeur
  UserInfo(this.username, this.password);
  UserInfo.userDetail(this.username,this.password,
      this.firstName,this.lastName,this.email,this.role);
  UserInfo.jsonLogin({this.username,this.password,this.email, this.accessToken,this.userId});
  UserInfo.jsonDetail({this.username, this.password, this.firstName, this.lastName, this.email,this.role, this.userId});
  //Fin Constructeur


  ///Fonction utilisé pour récupérer les données au format JSON en provenance du serveur
  ///les données sont directement enregistrées dans leurs variable correspodante.
  factory UserInfo.fromJson(Map<String,  dynamic> json){
    return UserInfo.jsonDetail(
      username: json['username'],
      password: json['password'],

      firstName: json['firstName'],
      lastName: json['lastName'],
      email: json['mail'],

      role: json['role'],
      userId: json['userId']
    );
  }


  ///Fonction utilisé pour récupérer les données au format JSON en provenance du serveur
  ///les données sont directement enregistrées dans leurs variable correspodante.
  factory UserInfo.fromJsonLogin(Map<String,  dynamic> json){
    return UserInfo.jsonLogin(
        username: json['username'],
        password: json['password'],

        email: json['email'],
        accessToken: json['accessToken'],
        userId: json['id'],


    );
  }
  ///Fonction utilisé pour envoyer les données au format JSON en direction du serveur
  Map<String, String> toJson() =>
      {
        "Cache-Control": 'no-cache',
        "Content-Type": "application/json",
        "Accept": "*/*",
        "Authorization":  'Bearer ' + accessToken.toString(),
        'userId' : userId.toString(),
      };

}




