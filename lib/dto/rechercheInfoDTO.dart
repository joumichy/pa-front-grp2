///  classe "RechercheInfo"

///

///  Cette classe permet la constitutiion ainsi que la reception d'éléments de recherches.

///  elle possède comme attribut :

///

/// String url : l'url youtube insérré par l'utilisateur.

/// String youtuber : le nom du youtuber insérré par l'utilisateur

/// String keywords : les mots clés insérrés par l'utilisateur

/// String catégoreis : les catégories selectionné par l'utilisateur

///

/// String username : l'username de l'utilisateur

/// int userID : id Unique de l'utilisateur

///

/// List<String> keywordsList : les mots clés enregistrés sous forme de liste

/// List<String> categoriesList : les catégories enregistrées sous forme de liste.

import 'userInfoDTO.dart';



class RecherheInfo{

  String url;
  String youtuber;
  String keywords;
  String categories;

  //Resultat ad titre;

  String username;
  int userid;

  List<String> keywordsList;
  List<String> categoriesList;

  String accessToken;

  //Result


  ///Setter de la classe.
  void setUrl(String url) => this.url = url;
  void setAccessToken(String accessToken) => this.accessToken = accessToken;
  void setYoutuber(String youtuber) => this.youtuber = youtuber;
  void setKeyWords(String keywords){
  this.keywords = keywords.toLowerCase();
  this.keywordsList = this.keywords.split(',');
  }
  void setCategories(String categories){
    this.categories = categories;
    if(categories != '%')  this.categoriesList = this.categories.split(',');


  }
  void setKeyWordsList(String keywords) => this.keywordsList = this.keywordsList;
  void setUsername(String username) => this.username = this.username;
  void setCategoriesListFromString(String categories) => this.categoriesList = this.categories.split(',');
  void setCategoriesList(List<String> categories) => this.categoriesList = this.categoriesList;
  void setUserID(int userid) => this.userid = userid;
  //Fin Setter




  ///Constructeur de la classe
  RecherheInfo(this.url, this.youtuber, this.keywords);
  RecherheInfo.YoutuberUrl(this.url,this.youtuber);
  RecherheInfo.setForResultat(this.userid,this.accessToken, this.youtuber, this.keywords, this.categories, this.url);
  //Fin Constructeur


  ///fonction ToJson utilisé lors des requêtes.
  /// les éléments insérrés sont utilisés pour effectuer le scraping.
  /// l'accesToken est ici utilisé pour se connecter au serveur.
  Map<String, String> toJson() =>
      {
        "Cache-Control": 'no-cache',
        "Content-Type": "application/json",
        "Accept": "*/*",
        "Authorization":  'Bearer ' + accessToken.toString(),

        'url_search': url,
        'youtubername_search' : youtuber,
        'category_search' : categories,
        'keywords_search' : keywords,


        'userId' : userid.toString(),
      };

  }

  ///Getter
  String get url => url;
  String get youtuber => youtuber;
  List<String> get keywords => keywords;
  //Fin getter


