/// *
/// classe "ResultatDTO"

///

/// cette classe récupère les résultats de la recherche

///

/// Elle est composée des attributs suivants :

///

///    String userResult : l'ID de l'utilisateur

///    String urlvideoSearch : url de insérré par l'utilisateur

///    String categorySearch : catégories insérrés par l'utilisateur

///    String youtuberNameSearch : nom du Youtubeur insérrés par l'utilisateur

///    String keyWordsSearch : mots clés insérrés par l'utilisateur

///    String urlvideoFind : url trouvé par le client Lourd

///    String categoryFind : catégories trouvées par le client Lourd

///    String youtuberNameFind : youtubeur trouvé par le client lourd

///    String keyWordsFind : mots clés trouvés par le client lourd

///    String urlVignette : image vignette trouvée par le client lourd

///

///    String accessToken : clé d'accès de l'utilisateur

///    int userId : id Unique de l'utilisateur

///

class ResultatDTO {

  String userResult;
  String urlvideoSearch;
  String categorySearch;
  String youtuberNameSearch;
  String keyWordsSearch;
  String urlvideoFind;
  String categoryFind;
  String youtuberNameFind;
  String keyWordsFind;
  String urlVignette;

  String accessToken;
  int userId;


  List<String> keywordsList = List<String>();
  List<String> categoriesList = List<String>();




  ///Setter
  void setKeyWordsListFromString(String keywords) =>
      {
        if(this.keyWordsFind.contains(',')){
          this.keywordsList = this.keyWordsFind.split(',')
        }
        else {
          this.keywordsList.add(this.keyWordsFind)

        }
      };

  void setKeyWordsList(String keywords) =>
      this.keywordsList = this.keywordsList;
  void setUserId(int userId) =>
      this.userId = userId;
  void setAccessToken(String accessToken) =>
      this.accessToken = accessToken;
  void setCategoriesListFromString(String categories) =>
      {
        if(this.categoryFind.contains(',')){
          this.categoriesList = this.categoryFind.split(',')
        }
        else {
          this.categoriesList.add(this.categoryFind)

        }
      };
  void setCategoriesList(List<String> categories) =>
      this.categoriesList = this.categoriesList;
    //Fin Setter

  ///Constructeur
  ResultatDTO.initUserInfo(this.userId, this.accessToken);
  ResultatDTO(this.youtuberNameFind, this.urlvideoFind, this.categoryFind,
      this.keyWordsFind);

  ResultatDTO.ResultatGET({this.userResult,
    this.urlvideoSearch, this.categorySearch, this.youtuberNameSearch,
    this.keyWordsSearch, this.urlvideoFind, this.categoryFind, this.youtuberNameFind,
    this.keyWordsFind, this.urlVignette});
  //Fin constructeur


  ///Fonction utilisé pour récupérer les données au format JSON en provenance du serveur
  ///les données sont directement enregistrées dans leurs variable correspodante.
  factory ResultatDTO.fromJson(Map<String, dynamic> json){
    return ResultatDTO.ResultatGET(



      urlvideoSearch: json['urlVideoSearch'],

      categorySearch: json['categorySearch'],

      youtuberNameSearch: json['youtuberNameSearch'],

      keyWordsSearch: json['keyWordsSearch'],

      urlvideoFind: json['urlvideoFind'],

      categoryFind: json['categoryFind'],

      youtuberNameFind: json['youtuberNameFind'],

      keyWordsFind: json['keyWordsFind'],

      urlVignette: json['urlVignette'],
    );
  }


  Map<String, String> toJson() =>
      {
      };



}
