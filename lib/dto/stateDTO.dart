/// Classe "StateDTO"

///

///  Cette classe permet de réceptionner l'état des recherches.

///

///  attribut :

///   - String statesearch_id: id Unique de l'état de la recherche

///   - int user_id; id Unique de l'utilisateur

///   - String search_id; id Unique de la recherche utilisateur effectuée

///   - String state; état de la recherche

import 'resultatDTO.dart';

class StateDTO{

 String statesearch_id;
 int user_id;
 String search_id;
 String state;


 ResultatDTO resultatDTO;



 ///Setter
 void setIdSearch(String statesearch_id) => this.statesearch_id = statesearch_id;
 void setIdUser(int user_id) => this.user_id = user_id;
 void setState(String state) => this.state = state;
 void setInfo(ResultatDTO info) => this.resultatDTO = resultatDTO;
  //Fin Setter

 ///Constructeur
  StateDTO(this.statesearch_id, this.user_id, this.state);
  StateDTO.GetState({this.statesearch_id, this.user_id, this.state, this.resultatDTO});
  //Fin Constructeur


 ///Fonction utilisé pour récupérer les données au format JSON en provenance du serveur
 ///les données sont directement enregistrées dans leurs variable correspodante.
  factory StateDTO.fromJson(Map<String, dynamic> json){
    return StateDTO.GetState(
      statesearch_id : json['stateSearch']['id'],
      user_id: json['stateSearch']['idUser'],
      state: json['stateSearch']['state'],

      resultatDTO: ResultatDTO.fromJson(json['searchResult'][0]),
    );
  }

 Map<String, String> toJson() =>
     {
     };


}
