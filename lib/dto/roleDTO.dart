/// Classe "Role"

///

///  Cette classe permet d'établir et de réceptionner les roles utilisateurs.

///

///  attribut :

///  - int id : ID uniue du Role

///  - String roleName; le nom du Role de l'utilisateur (ADMINISTRATEUR ou CLIENT)

class Role {
  int id;
  String roleName;

  ///Constructeur
  Role(this.id, this.roleName);
  Role.getJson({this.id, this.roleName});
  //fin Constructeur


  /// Permet de récupérer les données en provenance du serveur au format JSON
  /// et de les insérer directemnt dans leurs variable correspondante.
  factory Role.fromJson(Map<String, dynamic > json){
    return Role.getJson(
      id: json['id'],
      roleName: json['roleName'],
    );
  }

  Map<String, dynamic> toJson() =>
      {
        'id': id,
        'roleName' : roleName,

      };

}
