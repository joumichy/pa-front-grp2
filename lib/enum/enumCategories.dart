/**
 * l'énumérateur "enumCategories"
 * Regroupe l'ensemble des categories utilisé par l'application youtube.
 * Elle est ici utilisée pour permettre à l'utilisateur de préciser ses recherches
 * en selectionnant les catégories mises à disposition (cf catégoriesView.dart)

 */

enum enumCategories{
  BANDES_ANNONCES,
  EMISSIONS,
  COURT_METRAGE,
  THRILLER,
  SCIENCE_FICTION_FANTASTIQUE,

  HORREUR,
  ETRANGER,
  FAMILLE,
  DRAME,
  DOCUMENTAIRE,
  HUMOUR,
  CLASSIQUE,
  ACTION_AVENTURE,
  DESSINS_ANIMES_FILMS_ANIMATION,
  FILMS,
  SCIENCE_ET_TECHNOLOGIE,
  EDUCATION,
  VIE_PRATIQUE_ET_STYLE,
  ACTUALITES_ET_POLITIQUE,
  DIVERTISSEMENT,
  PEOPLE_ET_BLOGS,
  BLOGS_VIDEO,
  JEUX_VIDEO,
  VOYAGES_ET_EVENEMENTS,
  COURTS_METRAGES,
  SPORT,
  ANIMAUX,
  MUSIQUE,
  AUTO_MOTO,
  FILMS_ANIMATIONS

}
