/**
 * classe "FormMenu"
 *
 * Ces classes permettent à l'utilisateur d'effectuer ses recherches
 * pour obtenir les vidéos similaires à la vidéo insérée
 *
 * Elle va afficher une fenêtre composé du logo de l'application, ainsi que différents champs
 * que l'utilisateur devra remplir.
 *
 * les champs à remplir sont les suivants :
 *  -URL : champs où insérer l'url de la vidéo youtube
 *  -Youtuber Name: le nom du youtubeur
 *  -Mot Clés : les mots clés correspondant à ses idées de recherches
 *
 * Il y'aura en dessous deux bouttons selectionnable, les boutons :
 *
 * - Categories : pour ouvrir la fenêtre PopUpcategories et selectionnées ces dernières (cf PopUpcategories)
 * - Recherche : pour lancer la recherche au serveur
 *
 * routeName : '/menu'
 *
 *
 */
import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:frontproject/main.dart';

import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:validators/validators.dart';
import 'package:validators/sanitizers.dart';

import '../dto/resultatDTO.dart';
import '../dto/userInfoDTO.dart';
import 'categoriesView.dart';
import 'connectionView.dart';
import 'etatRechercheView.dart';
import 'parametreView.dart';
import '../dto/rechercheInfoDTO.dart';




class FormMenu extends StatefulWidget{
  static const routeName = '/menu';
  static const localhost = ConnexionRoute.localhost;
  static const apiResult = 'search/getresult';
  static const apiRechercheResult = 'search/newtask';

  Object arguments;
  @override
  State<StatefulWidget> createState() {

    return FormMenuState();
  }
}

class FormMenuState extends State<FormMenu>{
  var result;
  List<String> listCategories  = new List<String>();
  String url,youtuber,keywords, listSortedCategories;
  RecherheInfo recherheInfo = RecherheInfo(null,null,null);
  UserInfo userInfo;
  num idSearch = 0;
  List<RecherheInfo> listRechercheCreated = new List<RecherheInfo>();



  final TextEditingController urlControllers = TextEditingController();
  final TextEditingController youtuberController = TextEditingController();
  final TextEditingController keyWordsControllers = TextEditingController();



  final String champsInvalide = "Champs invalide !";
  final GlobalKey<FormState> formKeyMenu = GlobalKey<FormState>();


  ///Snacbar utilsé en fonction du resultat des requêtes
  final snackBarSuccess = SnackBar(content: Text('Nouvelle recherche enregistrée !'), backgroundColor: Colors.red,);
  final snackBarError = SnackBar(content: Text("Erreur lors de l'envoie"), backgroundColor: Colors.red,);
  final snackBarAlreadyMade = SnackBar(content: Text("Recherche déjà effectuée !"), backgroundColor: Colors.orange,);



  ///Champs insérable URL de la vidéo Youtube
  Widget buildUrlField(){
    return TextFormField(
      controller: urlControllers,
      decoration: InputDecoration(labelText: 'Exemple : Https://...'),

      validator: (String value){
        if(value.isEmpty){
          String response = champsInvalide;
          return response;
        }
        if(!isURL(value, requireTld: false)){
          String response = 'Url Invalide !';
          return response;
        }
        return null;
      },
      onSaved: (String value){
        url = value;
        recherheInfo.setUrl(url);
      },
    );
  }

  ///Button pour lancer la recherche
  Widget buildRechercheButton() {
    return Builder(builder: (context) =>RaisedButton(
      child: Text('Recherche',
      ),
      onPressed: ()  {
        log("Clicked Event Recherche :");
        if (formKeyMenu.currentState.validate())  {
          String url = urlControllers.text;

          recherheInfo = RecherheInfo.YoutuberUrl(url,
            youtuber,
          );
          recherheInfo.setYoutuber(youtuberController.text);
          recherheInfo.setCategoriesList(listCategories);
          recherheInfo.setKeyWords(keyWordsControllers.text);
          recherheInfo.setUrl(urlControllers.text);

          recherheInfo.setAccessToken(userInfo.accessToken);
          recherheInfo.setUserID(userInfo.userId);

          if(listCategories == null) recherheInfo.setCategories('%');
          else recherheInfo.setCategories(listCategories.toString().replaceAll('[', '').replaceAll(']', ''));


          //TODO add if success Scaffold Success
          Future<http.Response> info = postRechercheInfo(recherheInfo);
          info.then((value) => {
            if(value.statusCode == 201){


              Scaffold.of(context).showSnackBar(snackBarSuccess),

            }
            else if (value.statusCode == 302){
              Scaffold.of(context).showSnackBar(snackBarAlreadyMade),
            }
            else{
              Scaffold.of(context).showSnackBar(snackBarError),
            },
            log(value.body),
          });




        }
        formKeyMenu.currentState.save();
      },

    ) ,
    );
  }
  String sortList(String string){
    //TODO: NOT MANDATORY sort elem in order enum
    List list = string.split(',');
    for(String elem in list){
      log(elem.toString());
    }

  }



  Future<List<ResultatDTO>> getResult(RecherheInfo info) async {
    log("User TOKEN : "+ info.accessToken);
    log("User ID :" +info.userid.toString());

    log(info.categories);
    String newCategories = sortList(info.categories);

    final response = await http.get(MyApp.apiUrl+FormMenu.apiResult,
       headers: info.toJson()

    );

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      log('DATA');
      log((response.body).toString());
      //var data = List<RecherheInfo>();
      List<ResultatDTO> data = (json.decode(response.body) as List)
          .map((data) => ResultatDTO.fromJson(data))
          .toList();

      return data;
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load Result');
    }
  }
  Widget buildUserInfo(){
   if (ModalRoute.of(context).settings.arguments is UserInfo) userInfo = ModalRoute.of(context).settings.arguments;

    return Text('Effectuez une nouvelle recherche ! \n'+userInfo.username.toUpperCase(),textAlign: TextAlign.center, style:  TextStyle(
        color: Colors.black,
        fontWeight: FontWeight.w900,
        fontStyle: FontStyle.normal,
        fontFamily: 'Open Sans',
        fontSize: 15),);
  }
  Widget buildImageLogo(){


    return Image.asset(MyApp.appLogo, width: 200,  height: 150,);
  }


  Widget buildYoutuberInfo(){
    return TextFormField(

      decoration: InputDecoration(labelText: 'Exemple : Cyprien'),
      controller: youtuberController,
      validator: (String value){
        if(value.isEmpty){
          String response = champsInvalide;
          return response;
        }


        return null;
      },
      onSaved: (String value){
        youtuber = value;
        recherheInfo.setYoutuber(youtuber);
      },

    );
  }



  Widget buildCategories(){

    var data = listCategories;
    if(data.toString().replaceAll('[', '').replaceAll(']', '').isEmpty) return Text('');

    return Center(
      child: Text("Categorie : " + data.toString().replaceAll('[', '').replaceAll(']', ''),
      style:  TextStyle(
          color: Colors.black,
          fontWeight: FontWeight.w900,
          fontStyle: FontStyle.normal,
          fontFamily: 'Open Sans',
          fontSize: 14),),
    );

  }
  Widget buildCategoriesButton(){

    return RaisedButton(child: Text('Categories'),
      onPressed: () async {
      await showDialog(context: context,
      builder: (context) => PopUpcategories())
          .then((value) => {listCategories = value} );
      setState(() {});
      },
    );
  }

  Widget buildKeyWordsInfo(){
    return TextFormField(
      controller: keyWordsControllers,
      decoration: InputDecoration(labelText: 'Exemple : action, drame..'),
      validator: (String value){
        if(value.isEmpty){
          String response = champsInvalide;
          //Replace with ""
          return ;
        }


        return null;
      },
      onSaved: (String value){
        keywords = value;
        keywords = keyWordsControllers.text;
        recherheInfo.setKeyWords(keywords);
      },

    );
  }

  Widget buildIconParametre(){
    return IconButton(
      icon: const Icon(Icons.settings, color: Colors.black,),
      tooltip: 'Parametre',
      onPressed: (){
        Navigator.pushNamed(context, Parametre.routeName, arguments: userInfo);
      },
    );
  }

  Widget buildIconResultat(){
    return IconButton(
      icon: const Icon(Icons.notifications, color: Colors.black),
      tooltip: 'Etats des recherches',
      onPressed: (){
        Navigator.pushNamed(context, MenuResultat.routeName,arguments:userInfo);
      },
    );
  }


  @override
  Widget build(BuildContext context) {


    return Container(
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('image/background.jpg'), fit: BoxFit.cover
          )
      ),
      child: Scaffold(

        backgroundColor: Colors.transparent,
        appBar: AppBar(centerTitle: true,title: Text('Menu de Recherche', style: TextStyle(color: Colors.black),),
          automaticallyImplyLeading: false,
          backgroundColor: Colors.transparent,
          actions: <Widget>[
            buildIconParametre(),
            buildIconResultat(),
          ],),
        body: Container(
          margin: EdgeInsets.all(24),
          child: Form(
            key: formKeyMenu,
            child: ListView(

              //mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                buildImageLogo(),
                SizedBox(height: 5),
                buildUserInfo(),
                SizedBox(height: 5),

                buildUrlField(),
                SizedBox(height: 10),
                buildYoutuberInfo(),
                SizedBox(height: 10),
                buildKeyWordsInfo(),
                SizedBox(height: 10),
                buildCategories(),
                SizedBox(height: 10),
                buildCategoriesButton(),
                buildRechercheButton(),





              ],
            ),),
        ),

      ),
    ) ;
  }

  //============================================================================/
  //==============================FUNCTION======================================/
  //============================================================================/

  Future<http.Response> postRechercheInfo(RecherheInfo recherheInfo) async{

    //String api = 'api/auth/signin';

    final http.Response response = await http.post(MyApp.apiUrl+FormMenu.apiRechercheResult,
        headers: <String, String>{
          'Content-Type': 'application/json',

          "Authorization":  'Bearer ' + userInfo.accessToken.toString(),

        },
        body: jsonEncode(<String , dynamic>{
          'urlVideo' :  recherheInfo.url,
          'categoryVideo' : recherheInfo.categories,
          'youtuberName' : recherheInfo.youtuber,
          'keyWords'  : recherheInfo.keywords,
          'idUser'  : userInfo.userId,
        },)
    );
    if(response.statusCode == 201) {

      return response;

    }
    if(response.statusCode != 201 ) {
      log('ERREUR');
      log(response.body);
      log(response.statusCode.toString());
      return response;
    }

  }




}
