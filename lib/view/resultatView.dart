/**
 * classe "Resultats"
 *
 * Cette classe permet à l'utilisateur de visualiser le résultat de ses recherches
 * pour obtenir les vidéos similaires à la vidéo insérée
 *
 * Elle va afficher une fenêtre composé d'une liste d'éléments, correspondant aux informations
 * d'une vidéo youtube similaire aux informations d'une recherche effectuée.
 *
 * Pour se faire on va vérifier dans un premier temps si la recherche n'existe pas en dans le
 * système de persitence de données, pour ensuite aller récupérer les données dans la BDD via le serveur.
 *
 * L'utisateur aura la possibilité de cliquer sur l'un des éléments proposés pour visionner la vidéo
 * directement sur l'application Youtube.
 *
 *
 * routeName : '/resultat'
 *
 *
 */


import 'dart:convert';
import 'dart:developer';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:frontproject/dto/rechercheInfoDTO.dart';
import 'package:frontproject/dto/resultatDTO.dart';
import 'package:frontproject/main.dart';
import 'package:frontproject/view/etatRechercheView.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import 'menuRechercheView.dart';


class Resultats extends StatefulWidget{
  static const routeName = '/resultat';


  @override
  State<StatefulWidget> createState() {

    return ResultatsState();
  }
}


class ResultatsState extends State<Resultats> {

  List<ResultatDTO> infoList = new List<ResultatDTO>();
  Future<SharedPreferences> preferences = SharedPreferences.getInstance();



  ///Champs pour insérer du texte en fonction du contenu selectionné
  buildTextContent(String content){
    if (content == null || content.isEmpty) content = "";
    return Text(content,style: TextStyle(
        color: Colors.black,
        fontWeight: FontWeight.w900,
        fontStyle: FontStyle.normal,
        fontFamily: 'Open Sans',
        fontSize: 15),);
  }

  ///Champs pour affihcer les catégories
  buildListTextContent(List<String> content){
    String listToString = "Categories :";
    for(String elem in content){
      listToString += " "+elem;
    }
    return Text(listToString);
  }

  ///Champs pour Afficher la vignette de la vidéo
  buildImageVignette(String titre){

    //TODO : replace With Titre


    if(titre == null) titre = " ";
    return FadeInImage(
    image: NetworkImage(titre) ,placeholder: AssetImage(MyApp.imageError),width: 200,height: 150,);
  }


  ///Wiget pour créer une colonne contenant les différents informations d'une vidéo
  buildResultatYoutube(ResultatDTO dto){

    String url = dto.urlvideoFind;
    String titre = dto.urlVignette;
    String youtuber = dto.youtuberNameFind;
    dto.setKeyWordsListFromString(dto.keyWordsFind);
    dto.setCategoriesListFromString(dto.categoryFind);

    List<String> keywordsList = dto.keywordsList;
    List<String> categoriesList = dto.categoriesList;

    String userid = dto.userResult;

    String json = jsonEncode(dto);
    //echerheInfo info = RecherheInfo.dto(jsonDecode(json));

    return Column(
      children: <Widget>[
        SizedBox(height: 15),
        buildTextContent('URL: ' +url),
        buildImageVignette(titre),
        buildTextContent('Youtuber: '+ youtuber.toUpperCase()),
        //buildListTextContent(keywordsList),
        buildListTextContent(categoriesList),

        //buildTextContent('userID'),



      ],
    );
  }
  buildResultats(List<ResultatDTO> dto){

    return ListView.builder(
        scrollDirection: Axis.vertical,

        itemCount: dto.length,
        itemBuilder: (BuildContext context,int index){
          return new Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15.0),
              border: Border.all(
                color: Colors.black,
                width: 2
              )
            ),
            child: GestureDetector(

              onTap: () => launchURL(dto[index].urlvideoFind.toString()),//'https://www.google.com/'
              child:buildResultatYoutube(dto.elementAt(index)) ,
            ),
          );
        }
    );
  }

  launchURL(String url) async{
    if(await canLaunch(url)){
      await(launch(url));

    }
    else {
      throw 'Cannot Launch';
    }
  }
  @override
  Widget build(BuildContext context) {


    ResultatDTO resultat =  ModalRoute.of(context).settings.arguments;
    RecherheInfo recherheInfo = setRechercheInfo(resultat);

    Future<List<ResultatDTO>> dto = getResult(recherheInfo);
    dto.then((value) => infoList = value);

    return new Container(
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('image/background.jpg'), fit: BoxFit.cover
          )
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: AppBar(title: Text('Resultat des Recherches', style: TextStyle(color: Colors.black)),centerTitle: true,backgroundColor: Colors.transparent,),
        body: FutureBuilder(
          builder: (context, projectSnap){
            if(projectSnap.connectionState == ConnectionState.none &&
                projectSnap.data == null){
              return Container(child: Text('Error Loading'),);

            }
            if(projectSnap.connectionState == ConnectionState.waiting){
              return Center(child: MenuResultat.loadinglogo,);
            }
            return buildResultats(infoList);

          },
          future: dto,

        ),
      ),
    );
  }





  //////////////////////////////////////////////FUNCTION////////////////////////////

  ///Requetepour obtenir les résultats des recherches en fonction des éléments de recherches
  ///On s'assure que les données ne sont pas enregistrées dans le système de persistence de données
  Future<List<ResultatDTO>> getResult(RecherheInfo info) async {

    final SharedPreferences checkData = await preferences;

    //Création de la clé
    String key  = info.url+
        info.youtuber+
        info.keywords+
        info.categories;

    //Si la clées renvoie des données vides
    if(checkData.get(key) == null){

    final response = await http.get(MyApp.apiUrl+FormMenu.apiResult,
        headers: info.toJson());

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      //log('DATA');
      //log((response.body).toString());
      List<ResultatDTO> data = (json.decode(response.body) as List)
          .map((data) => ResultatDTO.fromJson(data))
          .toList();

      final SharedPreferences prefs = await preferences;
      prefs.setString(key
          , response.body).then((value) => {


      });



      return data;
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      log("ECHEC");
      throw Exception('Failed to load Result');
    }


    }else{
      log("ALREADY SAVED");
      var savedData = checkData.get(key);
      log(savedData);
      List<ResultatDTO> data = (json.decode(savedData) as List)
          .map((data) => ResultatDTO.fromJson(data))
          .toList();

      return data;
    }


    
      
   
  }

  RecherheInfo setRechercheInfo(ResultatDTO resultatDTO){

    RecherheInfo recherheInfo =
    new RecherheInfo.setForResultat(resultatDTO.userId,
        resultatDTO.accessToken,
        resultatDTO.youtuberNameSearch,
        resultatDTO.keyWordsSearch.toString(),
        resultatDTO.categorySearch.toString(),
        resultatDTO.urlvideoSearch);

    return recherheInfo;
  }
}
