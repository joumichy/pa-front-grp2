/**
 * classe "MenuResultat"
 *
 * Ces classes permettent à l'utilisateur d'effectuer ses recherches
 * pour obtenir les vidéos similaires à la vidéo insérée
 *
 * Elle va afficher une fenêtre composé d'une liste correspondant aux résultats des recherches.
 *
 * Chaque élément de la liste est composée d'un cercle de chargement se mettant à jour toutes les 3 versions
 *
 *
 * routeName : '/menuresultat'
 *
 *
 */


import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:frontproject/main.dart';
import '../dto/resultatDTO.dart';
import '../dto/stateDTO.dart';
import '../dto/userInfoDTO.dart';
import 'menuRechercheView.dart';
import '../dto/rechercheInfoDTO.dart';
import 'resultatView.dart';
import 'package:http/http.dart' as http;

class MenuResultat extends StatefulWidget{
  static const routeName = '/menuresultat';
  static const urlApiEtatRecherche = "search/getsearch/";

  static const loadinglogo = SpinKitPouringHourglass(
    color: Colors.indigo,
    size: 200,
  );


  @override
  State<StatefulWidget> createState() {

    return MenuResultatState();
  }
}

class MenuResultatState extends State<MenuResultat> {



  List<RecherheInfo> listRechercheCreated = new List<RecherheInfo>();
  StreamController streamController = StreamController();
  Timer timer;
  final snackBarError = SnackBar(backgroundColor :Colors.red,content: Text('Chargement incomplet !'));
  UserInfo userInfo ;



  ///Au lancement de la View, toutes les 3 secnondes on renouvelle la requête.
  @override
  void initState(){

    getPeriodicData(userInfo);

    timer = Timer.periodic(Duration(seconds: 3 ), (timer) =>  getPeriodicData(userInfo));

    super.initState();
  }

  @override
  void dispose(){
    if( timer.isActive) timer.cancel();

    super.dispose();
  }

  ///Widget pour construire le cercle de chargement
  Widget buildProgressBar(double value,String data){
    var textLoading = "Chargement en cours...";
    IconData icon = Icons.access_time;
    if(value == 1.0){
      textLoading = "Complete";
      icon = Icons.done;
    }
    return Container(
        margin: EdgeInsets.symmetric(vertical: 20.0),
        height: 50.0,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: <Widget>[
          Center(
            child:Text(data, style:  TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.w900,
                fontStyle: FontStyle.normal,
                fontFamily: 'Open Sans',
                fontSize: 15),),
          ),

          SizedBox(width: 20),
          CircularProgressIndicator(
            value: value,
            strokeWidth: 7.0,
            backgroundColor: Colors.black,
            valueColor: AlwaysStoppedAnimation<Color>(Colors.indigo),
          ),
          SizedBox(width: 30 ),

          Icon(icon,size: 30, color: Colors.green,)

        ],
      )


    );
  }

  

  @override
  Widget build(BuildContext context) {

    userInfo = ModalRoute.of(context).settings.arguments;
    ResultatDTO data = ResultatDTO.initUserInfo(userInfo.userId, userInfo.accessToken);

    return Container(
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('image/background.jpg'), fit: BoxFit.cover
          )
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: AppBar(title: Text('Etat des recherches', style: TextStyle(color: Colors.black)),centerTitle: true, backgroundColor: Colors.transparent,),
        body: StreamBuilder(

          stream: streamController.stream,
          builder: (BuildContext context, AsyncSnapshot snapchot){

            if(snapchot.hasData){
              List<StateDTO> list = snapchot.data as List<StateDTO>;
              if(list.length == 0){
                timer.cancel();
                return Center(
                child: Text('Aucune recherche effectuée !'),
                );
              }
              return ListView.builder(
                scrollDirection: Axis.vertical,
                itemCount: list.length,
                padding: EdgeInsets.all(16.0),
                itemBuilder: (BuildContext context, int index){
                  return new GestureDetector(
                    onTap: () => {
                      log('Vous avez cliqué sur ' + list[index].statesearch_id.toString()),
                      //log("DTO : " + list[index].resultatDTO.urlvideoSearch),
                      if(list[index].state == '100'){
                        data = list[index].resultatDTO,
                        //log(list[index].resultatDTO.urlvideoSearch.toString()),
                        data.setAccessToken(userInfo.accessToken),
                        data.setUserId(userInfo.userId),
                        Navigator.pushNamed(context,Resultats.routeName,arguments: data),
                        timer.cancel(),
                      }
                      else{
                        Scaffold.of(context).showSnackBar(snackBarError),
                      }

                    },
                    child: buildProgressBar( stateToDouble(list[index].state),
                        'iD de Recherche: '+ list[index].statesearch_id.toString()),

                  );
                },
              );



            }

            return Center(
              child: MenuResultat.loadinglogo,
            );
          },
        ),
      ),
    );


  }






  ////////////////////////////FUNCTION/////////////////////////:

  ///On effectue un GET au serveur pour mettre à jour de manière périodique
  ///la barre de chargement de la recherche.
  Future<List<StateDTO>>getPeriodicData(UserInfo info) async{

    userInfo = ModalRoute.of(context).settings.arguments;
    log('USER ID :' + userInfo.userId.toString());

   String stateSearchRoute = MenuResultat.urlApiEtatRecherche+ userInfo.userId.toString();

    log('Link : '+MyApp.apiUrl+stateSearchRoute);
   final response = await http.get(MyApp.apiUrl+stateSearchRoute,headers: userInfo.toJson());

   var data = new List<StateDTO>();
   if(response.statusCode == 500){
      //NO DATA
   }

   else{
     log('STATUT CODE :' + response.statusCode.toString());
     log('Resultat requete : ' + response.statusCode.toString() +"\n Message :" + response.body);
     data = (json.decode(response.body) as List)
         .map((data) => StateDTO.fromJson(data))
         .toList();

   }


   log('Resultat requete : ' + response.statusCode.toString() +"\n Message :" + response.body);

   streamController.add(data);


  }

  ///On transforme l'état des recherches en valeur pour le cercle de chargement
  double stateToDouble(String state){
    double result = 0;

    switch(state){
      case "10": result = 0.10; break;
      case "20": result = 0.20; break;
      case "30": result = 0.30; break;
      case "50": result = 0.50; break;
      case "70": result = 0.70; break;
      case "80": result = 0.80; break;
      case "90": result = 0.90; break;
      case "100": result = 1.00; break;
    }

    return result ;
  }
}

