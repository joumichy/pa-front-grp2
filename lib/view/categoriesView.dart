/// classe "PopUpcategorie"

///

/// Cette classe permet à l'utilisateur de selectionner les catégories qu'il désire

/// pour accentuer ses éléments de recherches.

///

/// Elle va ouvrir une fenetre de type "Pop Up",

/// et ainsi réveler un ensemble de Checkbox selectionnable de manière dynamique.

/// Ces checkbox enregistrés seront renvoyés à la view "FormMenu" dans le fichier "MenuRechercheView"

///

/// routeName : '/categories'

///

///


import 'dart:developer';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:grouped_buttons/grouped_buttons.dart';
import '../enum/enumCategories.dart';


class PopUpcategories extends StatefulWidget {
  //final MyFormCallback onSubmit;
  static const routeName = '/categories';

  PopUpcategories();

  @override
  PopUpcategoriesState createState() => PopUpcategoriesState();
}

class PopUpcategoriesState extends State<PopUpcategories> {
  List<String> data = new List<String>();
  @override
  Widget build(BuildContext context) {

    return AlertDialog(backgroundColor : Colors.white70,  title: Text('Categorie'),
    content: buildListCategory(),
      actions: <Widget>[
        FlatButton(
          child: Text('Enregistrer'),
          onPressed: () {
            ///Renvoie des données à la view Père.
            Navigator.of(context).pop(data);

          },
        ),
      ],);
  }

  ///Creation du Widget qui contiendra la liste des catégories
  Widget buildListCategory(){
    List<String> stringCategories = new List<String>();
    for (int i = 0; i <enumCategories.values.length;i++){

      //Retrait des caractère non pertinent de l'enum
      String elem = enumCategories.values.elementAt(i)
          .toString()
          .split('.')[1]
          .toLowerCase().replaceAll('_', ' ');


      stringCategories.add(elem);
    }
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      padding: const EdgeInsets.all(1.0),
      child:
        SingleChildScrollView(scrollDirection: Axis.vertical,
        child: CheckboxGroup(
          labels: stringCategories,
          onSelected: (List<String> checked) =>{ print (checked.toString()),
          //TODO: NOT MANDATORY Align Checkbox
            data = checked.toList()

          },

        ), )
    );

  }
}
