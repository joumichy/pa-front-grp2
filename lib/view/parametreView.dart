/// classe "Parametre"
///
/// Ces classes permettent à l'utilisateur d'afficher les information utilisateur
/// ainsi que de se déconnecter
///
/// Elle va afficher le nom d'utilisateur aini que son adresse email.
///
/// Il y'aura en dessous un boutton selectionnable :
///
/// - Se déconnecter : permet de se déconnecter de l'application et d'être redirigée vers la vue de Connexion utilisateur
///
///
/// routeName : '/parametre'
///
///

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../dto/userInfoDTO.dart';
import 'connectionView.dart';


class Parametre extends StatefulWidget{
  static const routeName = '/parametre';

  @override
  State<StatefulWidget> createState() {

    return ParametreState();
  }
}

class ParametreState extends State<Parametre> {
  UserInfo userInfo;

  Widget buildDataUser(String info){
    return Text(info, textAlign: TextAlign.center,style: TextStyle(color: Colors.black, fontSize: 25,fontWeight: FontWeight.bold));
  }

  Widget buildLogout(){
    //TODO: Stop ALL Activity
    return Center(
      child: RaisedButton(
        child: Text('Se Deconnecter'),
        onPressed: () => {
          userInfo.accessToken = null,

        Navigator.of(context).pushNamedAndRemoveUntil(ConnexionRoute.routeName, (Route<dynamic> route) => false),

      },
      ),
    );
  }


  @override
  Widget build(BuildContext context) {
    userInfo = ModalRoute.of(context).settings.arguments;
    return Container(
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('image/background.jpg'), fit: BoxFit.cover
          )
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: AppBar(title: Text('Parametre Utilisateur', style: TextStyle(color: Colors.black)),centerTitle: true,backgroundColor: Colors.transparent,),
        body: Container(
          margin: EdgeInsets.all(24),
          child: ListView(
            children: <Widget>[
              buildDataUser(userInfo.username),
              SizedBox(height: 10,),
              buildDataUser(userInfo.email),
              SizedBox(height: 50,),

              buildLogout(),
            ],
          ),
        ),
      ),
    );
  }

}
