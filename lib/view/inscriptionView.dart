/// classe "FormScreen"
///
/// Cette classe permette à l'utilisateur de s'inscire à l'application
///
/// Elle va afficher une fenêtre composé du logo de l'application, ainsi que de six champs
/// que l'utilisateur devra remplir.
///
/// les champs à remplir sont les suivants :
///  - Nom : champs où insérer le nom de famille de l'utilisateur
///  - Prénom : champs où insérer le prénom de l'utilisateur
///  - Email : champs où insérer l'email de l'utilisateur
///  - Nom d'utilisateur : champs où insérer le nom d'utilisateur
///  - Mot de Passe : champs où insérer le mot de passe de l'utilisateur
///  - Confirmer Mot de Passe : champs où insérer le mot de passe utilisateur une seconde fois
///
///   Il y'aura en dessous un bouton pour envoyer le formulaire d'inscription :
///
/// - Inscription : pour inscrire l'utilisateur à l'application.
///
///  routeName : '/inscription'
///


import 'dart:convert';
import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:frontproject/main.dart';
import 'package:http/http.dart' as http;
import 'package:email_validator/email_validator.dart';
import '../dto/userInfoDTO.dart';
import 'connectionView.dart';

class FormScreen extends StatefulWidget{
  static const routeName = '/inscription';
  static const urlApiInscription = 'user/signup';
  @override
  State<StatefulWidget> createState() {
  
    return FormScreenState();
  }

  

}

class FormScreenState extends State<FormScreen>{

  String firstName,lastName,email,username,password,confirmPassword;
  final snackBarSuccess = SnackBar(content: Text('Inscription Successful !'), backgroundColor: Colors.red,);
  final snackBarAlreadyExist = SnackBar(content: Text('Utilisateur déjà inscrit !'), backgroundColor: Colors.red);
  final snackBarError = SnackBar(content: Text('Problème de Connexion.. !'), backgroundColor: Colors.red);
  final TextEditingController lastNameController = TextEditingController() ,
      firstNameController = TextEditingController(),
      emailController = TextEditingController(),
      usernameController = TextEditingController(),
      passwordController = TextEditingController(),
      confirmPasswordController = TextEditingController();
  
  final String champsObligatoire = 'Champs Obligatoire !';

  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  ///Widget pour insérrer nom de famille utilisateur
  Widget buildNomField(){
    return TextFormField(
      decoration: InputDecoration(labelText: 'Nom'),
      controller: lastNameController,
      validator: (String value){
        if(value.isEmpty){
          String response = champsObligatoire;
          return response;
        }
        return null;
      },
      onSaved: (String value){
        lastName = value;
      },
    );
  }

  ///Widget pour insérrer le prénom utilisateur
  Widget buildPrenomField(){
    return TextFormField(
      controller: firstNameController,
      decoration: InputDecoration(labelText: 'Prenom'),
      validator: (String value){
        if(value.isEmpty){
          String response = champsObligatoire;
          return response;
        }
         return null;
      },
      onSaved: (String value){
        firstName = value;
      },
    );
  }

  ///Widget pour insérrer l'émail utilisateur
  Widget buildEmailField(){
    return TextFormField(
      controller: emailController,
      decoration: InputDecoration(labelText: 'Email'),
      validator: (String value){
        if(value.isEmpty){
          String response = champsObligatoire;
          return response;
        }
        if(!EmailValidator.validate(value,true)){
          String response = 'Email incorrecte';
          return response;
        }
         return null;
      },
      onSaved: (String value){
        email = value;
      },
    );
  }

  ///Widget pour insérrer nom de  l'utilisateur
  Widget buildNomUtilisateurField(){
    return TextFormField(
      controller: usernameController,
      decoration: InputDecoration(labelText: "Nom d'utilisateur"),
      validator: (String value){
        if(value.isEmpty){
          String response = champsObligatoire;
          return response;
        }

         return null;
      },
      onSaved: (String value){
        username = value;
      },
    );
  }

  ///Widget pour insérrer le mot de passe utilisateur
  Widget buildMotDePasseField(){
    return TextFormField(
      controller: passwordController,
      decoration: InputDecoration(labelText: 'Mot de Passe' ),
      obscureText: true,
      validator: (String value){
        if(value.isEmpty){
          String response = champsObligatoire;
          return response;
        }
         return null;
      },
      onSaved: (String value){
        password = value;
      },
    );
  }
  ///Widget pour confirmer le mot de passe de utilisateur
  Widget buildConfirmMotDePasseField(){
    return TextFormField(
      controller: confirmPasswordController,
      decoration: InputDecoration(labelText: 'Confirmer votre Mot de Passe' ),
      obscureText: true,
      validator: (String value){
        if(value.isEmpty){
          String response = champsObligatoire;
          return response;
        }
        if(value != passwordController.text){
          return 'Mot de passe différent';

        }
        return null;
      },
      onSaved: (String value){
        password = value;
      },
    );
  }


  ///Widget pour afficher le logo de l'application
  Widget buildImageLogo(){

    return Image.asset(MyApp.appLogo, width: 200, height: 150,);
  }

  ///Widget pour envoyer le formulaire formater en JSON au serveur
  Future<http.Response> userInscriptionPOST(UserInfo userInfo) async{
    String api = FormScreen.urlApiInscription;

    log(userInfo.username);
    log(userInfo.email);
    final http.Response response = await http.post(MyApp.apiUrl+api,
      headers: <String, String>{
        'Content-Type': 'application/json',
      },

      body: jsonEncode(<String, dynamic>{
        "username": userInfo.username,
        "password":userInfo.password,
        "firstName":userInfo.firstName,
        "lastName":userInfo.lastName,
        "mail":userInfo.email,
        "role":userInfo.role,
      }),
    );
    if(response.statusCode == 201 ) {
      log('Inscription Reussie');
      return response;
    }

    if(response.statusCode == 401 ) {
      log('Inscription Echoué');
      return response;
    }

    if(response.statusCode == 400 ) {
      log("L'utilisateur existe déjà !");
      log(response.body);
      return response;
    }

  }


  ///Widget Build general contenant tous les Widgets de l'application
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('image/background.jpg'), fit: BoxFit.cover
          )
      ),
      child: Scaffold(
          backgroundColor: Colors.transparent,
          appBar: AppBar(title: Text('Inscription Utilisateur', style: TextStyle(color: Colors.black)),backgroundColor: Colors.transparent,),
          body: Container(
            margin: EdgeInsets.all(24),
            child: Form(
              key: formKey,
              child: ListView(
                scrollDirection: Axis.vertical,
                children: <Widget>[
                  buildImageLogo(),
                  SizedBox(height: 20),
                  buildNomField(),
                  SizedBox(height: 10),
                  buildPrenomField(),
                  SizedBox(height: 10),
                  buildEmailField(),
                  SizedBox(height: 10),
                  buildNomUtilisateurField(),
                  SizedBox(height: 10),
                  buildMotDePasseField(),
                  SizedBox(height: 10),
                  buildConfirmMotDePasseField(),

                  SizedBox(height: 24),
                  Builder(builder: (context) => RaisedButton (
                    child: Text(
                      "S'inscrire",
                    ),

                    onPressed: () {
                      final snackBar = SnackBar(content: Row(
                        children: <Widget>[
                          Container(child: ConnexionRoute.loadinglogo, width: 30, height: 30,),
                          SizedBox(width: 10),
                          Text('Verification des informations...')
                        ],
                      )
                      );



                      if(formKey.currentState.validate()){

                        String role;
                        role = 'user';
                        UserInfo userDetail = new UserInfo.userDetail(
                            usernameController.text,
                            passwordController.text,
                            firstNameController.text,
                            lastNameController.text,
                            emailController.text,role );


                        Future<http.Response> response;
                        response = userInscriptionPOST(userDetail);
                        Scaffold.of(context).showSnackBar(snackBar);
                        response.then((value) =>{

                          if(value.statusCode == 201) {

                            Scaffold.of(context).showSnackBar(snackBarSuccess),
                            Navigator.of(context).pop(),
                          }

                          else{
                            if(value.statusCode == 400){
                                Scaffold.of(context).showSnackBar(snackBarAlreadyExist),

                            }
                          }
                        });
                      }

                      formKey.currentState.save();



                    },
                  ),

                  )
                ],


              ),
            ),

          )

      ),
    );
  }


}
