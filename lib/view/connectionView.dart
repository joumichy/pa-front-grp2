/// classe "ConnexionRoute"/ "FormConnexion"

///

/// Ces classes permettent à l'utilisateur de se connecter à l'application ou

/// d'accèder à la View d'inscription utilisateur

///

/// Elle va afficher une fenêtre composé du logo de l'application, ainsi que deux champs

/// que l'utilisateur devra remplir.

/// les champs à remplir sont les suivants :

///  -Username : champs où insérer le nom d'utilisateur

///  -Mot de Passe : champs où insérer le mot de passe utilisateur

/// Il y'aura en dessous deux bouttons selectionnable, les boutons :

///

/// - Connexion : pour connecter l'utilisateur à l'application

/// - Inscription : pour inscrire l'utilisateur à l'application.

///

/// routeName : '/connexion'

///

///

import 'dart:convert';
import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:frontproject/main.dart';
import 'package:frontproject/view/etatRechercheView.dart';
import 'package:http/http.dart' as http;
import '../dto/userInfoDTO.dart';
import 'inscriptionView.dart';
import 'menuRechercheView.dart';


class ConnexionRoute extends StatelessWidget{
  static const ipv4 = "192.168.43.114";//"192.168.1.38";
  static const localhost = 'http://'+ipv4+":8080/";
  static const routeName = '/connexion';
  static const urlApi = "auth/signin";

  static const loadinglogo = SpinKitWave(
    color: Colors.indigo,
    size: 20,
  );
  @override
  Widget build(BuildContext context) {
    
    return FormConnexion();
  }

}

class FormConnexion extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return FormConnexionState();
      }
      
    }
    

class FormConnexionState extends State <FormConnexion>{

  String username, password;
  final String champsInvalide = "Champs invalide !";
  final GlobalKey<FormState> formKeyConnection = GlobalKey<FormState>();
  bool connected = false;


  //Controller
  final usernameController = TextEditingController();
  final passwordController = TextEditingController();

  final snackBar = SnackBar(backgroundColor: Colors.red,content: Row(
    children: <Widget>[
      Container(child:ConnexionRoute.loadinglogo,width: 30,height: 30 ,),
      SizedBox(width: 10),
      Text('Connexion en cours...')
    ],
  )
  );

  final snackBarError = SnackBar(content: Text('Erreur de Connexion'));

    ///Widget pour insérrer nom d'utilisateur
    Widget buildUsernameField(){
    return TextFormField(
      controller: usernameController,

      decoration: InputDecoration(icon: Icon(Icons.account_circle,color: Colors.indigo,),
          labelText: 'Username'),
      validator: (String value){
        if(value.isEmpty){
          String response = champsInvalide;
          return response;
        }
         return null;
      },
      onSaved: (String value){
        username = value;
      },
    );
  }


  ///Widget pour insérrer le mot de passe utilisateur
   Widget buildPasswordField(){
    return TextFormField(

      controller: passwordController,

      decoration: InputDecoration(icon: Icon(Icons.lock, color: Colors.indigo,), labelText: 'Password',),
      obscureText: true,
      validator: (String value){
        if(value.isEmpty){
          String response = champsInvalide;
          return response;
        }
         return null;
      },
      onSaved: (String value){
        username = value;
      },
    );
  }

  ///fonction pour établir la connexion au serveur,
  ///On y envoie l'username ainsi que son mot de passe
  ///en cas de succès :
  /// - une clé d'accès est fournie et l'application
  ///redigerera l'utilisateur vers la vue de Recherche.
  ///En cas d'echec :
  /// - Un Popup snackbar est affiché pour indiqué à l'utiisateur qu'il y'a une erreur
  Future<UserInfo> userConnexionPOST(UserInfo userInfo) async{
      String api = ConnexionRoute.urlApi;
      final http.Response response = await http.post(MyApp.apiUrl+api,
          headers: <String, String>{
            'Content-Type': 'application/json',
          },
        body: jsonEncode(<String, String>{
          'username': userInfo.username,
          'password': userInfo.password,
        }),
      );
      if(response.statusCode == 200 ) {
        connected = true;
        return UserInfo.fromJsonLogin(json.decode(response.body));
      }

      else if(response.statusCode == 401 ) {
        Scaffold
            .of(context)
            .showSnackBar(SnackBar(content: Text('Unauthorized')));
        log("Erreur");
      }
      else{
        Scaffold
            .of(context)
            .showSnackBar(SnackBar(content: Text("Erreur connexion")));
      }

  }

    ///Button de connection, lorsque l'on clique dessus,
  ///les données utilisateur inscrite sont récupérées,
  ///la fonction userConnexionPOST est ensutie déclenchées.
   Widget buildConnectionButton(){
    return Builder(builder: (context) => RaisedButton(
      child: Text('Se Connecter' ,),
      onPressed: (){




        log("Clicked Event Connexion " + MyApp.apiUrl+'api/auth/signin');

        if(formKeyConnection.currentState.validate()){


          username = usernameController.text;
          password = passwordController.text;
          UserInfo userInfo = new UserInfo(username, password);
          Future<UserInfo> response;

          response = userConnexionPOST(userInfo);
          Scaffold.of(context).showSnackBar(snackBar);

          response.then((value) => {
            if(connected) {
              userInfo = value,
              log('Connexion Succesful, User ID : '+value.userId.toString()),

              Navigator.pushReplacementNamed(context, FormMenu.routeName,arguments: userInfo),
            }
            else {
              Scaffold.of(context).showSnackBar(snackBarError),
            }



          });



        }
        else return;


      },
    ),

    );
  }

  ///Button d'inscription,
  ///lorque l'on clique dessus, l'utilisateur sera redirigé
  ///vers la View d'inscription.
  Widget buildInscriptionButton(){
    return RaisedButton(
      child: Text('Creer un compte' ,),
      onPressed: (){
        
        log("Clicked Event Inscription");
        Navigator.pushNamed(context,FormScreen.routeName,
        );
         },
    );
  }

  ///Widget pour afficher le logo de l'application
  Widget buildImageLogo(){

      return Image.asset(MyApp.appLogo,width: 200,height: 150,);
  }



  ///Build MAIN contenant tous les widget de la view.
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('image/background.jpg'), fit: BoxFit.cover
          )
      ),
      child: Scaffold(

        appBar: AppBar( backgroundColor: Colors.transparent, centerTitle: true,
          title:  Text("Improved Youtube search",textAlign: TextAlign.center, style: TextStyle(color: Colors.black)),),
          backgroundColor: Colors.transparent,
          body: Container(
              margin: EdgeInsets.all(24),
              child: Form
                (key: formKeyConnection,
                child: ListView(
                  scrollDirection: Axis.vertical,
                  children: <Widget>[
                    buildImageLogo(),
                    SizedBox(height: 50,),
                    buildUsernameField(),
                    SizedBox(height: 24),
                    buildPasswordField(),
                    SizedBox(height: 24),
                    buildConnectionButton(),
                    buildInscriptionButton(),

                  ],
                ),
              )
          )
      ),
    );
  }
}
