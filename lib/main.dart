///
/// Classe "MyApp"
///
/// Représentant le main de l'application, elle dispose de toutes les routes pour
/// rediriger les views, ainsi que des éléments pour le design du Front .
///
/// Cette classe appel dans son build la View Connexion pour rediriger l'utlisateur
/// au Menu de connexion.

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'view/categoriesView.dart';
import 'view/connectionView.dart';
import 'view/etatRechercheView.dart';
import 'view/inscriptionView.dart';
import 'view/menuRechercheView.dart';
import 'view/parametreView.dart';
import 'view/resultatView.dart';


void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  static const appLogo = 'image/app-logo.png';
  static const imageError = 'image/imagefailed.png';
  //static const apiUrl = "http://192.168.43.114:8080/";//http://192.168.1.38:8080/";//"https://grp2-server-pa.herokuapp.com/";
  static const apiUrl = "https://grp2-server-pa.herokuapp.com/";

  @override
  Widget build(BuildContext context) {
    String apptitle = 'Improved Youtube Search';

        return MaterialApp(

          routes: {
            ConnexionRoute.routeName : (context) => ConnexionRoute(),
            FormScreen.routeName : (context) => FormScreen(),
            FormMenu.routeName : (context) => FormMenu(),
            PopUpcategories.routeName : (context) => PopUpcategories(),
            Resultats.routeName: (context) => Resultats(),
            MenuResultat.routeName: (context) => MenuResultat(),
            Parametre.routeName: (context) => Parametre(),
          },

          title: apptitle,
            /// élément de Design
          theme: ThemeData(

            buttonTheme: ButtonThemeData(
              buttonColor: Colors.indigo,
              textTheme: ButtonTextTheme.primary,

              shape:  RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18.0),
                  side: BorderSide(color: Colors.red)
              ),
            ),
            cursorColor: Colors.indigo,
            iconTheme: IconThemeData(color: Colors.indigo),
            appBarTheme: AppBarTheme(
              color: Colors.indigo
            ),
            inputDecorationTheme: InputDecorationTheme(
              labelStyle: TextStyle(color: Colors.indigo,fontSize: 14),
              enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(100.0),
                  borderSide: const BorderSide(color: Colors.black, width: 2.0)
              ),

              errorBorder:  OutlineInputBorder(
                  borderSide: const BorderSide(color: Colors.redAccent, width: 2.0),
                  borderRadius: BorderRadius.circular(100.0)),

              focusedErrorBorder: OutlineInputBorder(
                  borderSide: const BorderSide(color: Colors.redAccent, width: 2.0),
                  borderRadius: BorderRadius.circular(100.0)),

              border: OutlineInputBorder(
                    borderSide: const BorderSide(color: Colors.black, width: 2.0),
                    borderRadius: BorderRadius.circular(100.0),
                  ),
              focusedBorder: OutlineInputBorder(
                borderSide: const BorderSide(color: Colors.indigo, width: 2.0),
                borderRadius: BorderRadius.circular(100.0),
              ),
            ),

          ),
          
          home:Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('image/background.jpg'), fit: BoxFit.cover
              )
            ),
            child:ConnexionRoute(),
          )


  
    );
  }
}
