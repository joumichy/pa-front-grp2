
# Improved Youtube Search

## Summary
Ce présent document représente le Readme du front de l'application du groupe 2 4AL projet annuel 2019-2020.

Cette application permet de réaliser un scrapping youtube en fonction d'une vidéo insérer par l'utilisateur
avec différents éléments de recherches mis à dispositon, tel que le nom du youtubeur, des mots de clés ou encore des catégories définies.

il en ressortira des vidéos similaires suite à une analyse effectuée par un client lourd exploitant l'API Youtube.

## Installation

Afin d'utiliser l'application, clonez ce dernier projet en veillant d'avoir installé flutter (Lien : https://flutter.dev/docs/get-started/install)

## Utilisation de l'application

Cette application nécéssite la création d'un compte utilisateur. 

Une fois votre compte crées, vous pouvez connecter et ainsi insérer dans les champs dédiés vos éléments de recherches :

- URL
- Nom Youtubeur
- Mots Clés
- Categories.

Une fois votre recherche lancées vous pourrez accéder à l'état d'avancement de vos recherches en cliquant sur la petite clochette en haut à droite de votre écran.

Un cercle de chargement sera mis à disposition pour chacune de vos recherches éffectuées. Elles symbolisent l'état d'avancement de l'analyse executé par le client lourd.

Une fois le chargement terminé vous pourrez accéder au résultat de l'analyse avec au plus 10 vidéos correspondant au résultat de l'analyse.

Il vous suffit de cliquer dessus pour visionner la vidéo  !

Enjoy.

Auteur : Groupe 2 ESGI 4AL 2019-2020 (ALLOU John, GHALEM Marc, GOURDON Valentin).


